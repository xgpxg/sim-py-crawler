# sim-py-crawler

## 介绍

python爬虫包

 **注意：仅供学习交流使用，切勿非法爬取数据；合理控制爬取间隔时间，避免对源站造成过大影响** 

## 支持内容

### 豆瓣影评爬虫

 **采集字段：** 用户名、评分、点赞数、日期、评论内容

 **说明：** 由于豆瓣限制，未登录用户只能查看前20页评论内容，如果要采集更多内容，请先登录。
 
 **示例：** 



```
from douban.crawler import DouBanCrawler

if __name__ == '__main__':
    dbc = DouBanCrawler(30, 0.2)
    #登录
    dbc.login('手机号', '密码')
    #获取数据
    data_list = dbc.get_comment(25853071)
    #保存到CSV
    dbc.save_to_csv(data_list, 'D:/study/crawler_data/豆瓣影评/庆余年豆瓣影评.csv')

```


![输入图片说明](https://images.gitee.com/uploads/images/2020/0109/103933_68c421a0_1537128.png "屏幕截图.png")


### 知乎问题爬虫

 **采集字段：** 用户名、用户签名、点赞数、回答内容

 **说明：** 采用半自动模式（需要手动扫码登陆），需要开启浏览器显示；分页参数等同于下拉到底部后异步加载数据的次数
 
 **示例：** 

````
from zhihu.crawler import ZhiHuCrawler

if __name__ == '__main__':
    zhc = ZhiHuCrawler(30, 5,True)
    zhc.login()
    data_list = zhc.get_answer(360528621)
    zhc.save_to_csv(data_list, 'D:/study/crawler_data/知乎/电视剧《庆余年》中有什么细思极恐的情节.csv')
````

![输入图片说明](https://images.gitee.com/uploads/images/2020/0109/224939_a337cf1a_1537128.png "屏幕截图.png")